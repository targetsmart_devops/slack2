"""
Pertinent bits of information about this project
"""

from setuptools import setup

setup(
    name='slack2',
    packages=['slack2'],
    version='2.9.1',
    description='Send text and files to slack; plus exception notification.',
    author='Jack Desert',
    author_email='jack.desert@targetsmart.com',
    url='https://bitbucket.org/targetsmart_devops/slack2.git',
    download_url='https://bitbucket.org/targetsmart_devops/slack2.git',
    keywords=['slack', 'upload', 'exception-notification'],
    classifiers=[],
    # Python 3.7 or greater is required in order to support the type annotations we are using
    python_requires='>=3.7',
    install_requires=[
        'boto3>=1.5.0',
        'requests>=2.22.0',
        'tenacity>=7.0.1',
        'python-dateutil>=2.7.4',
    ],
)
