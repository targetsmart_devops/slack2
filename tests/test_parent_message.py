from __future__ import annotations

from slack2.parent_message import ParentMessage


def test_from_string_1():
    """
    Happy Path
    """
    parent = ParentMessage.from_string('a,b')
    assert parent.channel == 'a'
    assert parent.thread_ts == 'b'


def test_from_string_2():
    """
    When text is None
    """
    parent = ParentMessage.from_string(None)
    assert parent is None


def test_from_string_3():
    """
    When text contains no commas
    """
    parent = ParentMessage.from_string('no-comma')
    assert parent is None


def test_from_string_4():
    """
    When text contains multiple commas
    """
    parent = ParentMessage.from_string('some-channel,some-thread-ts,something-else')
    assert parent is None


def test_from_string_5():
    """
    When text is not a string
    """
    parent = ParentMessage.from_string(['list', 'of', 'strings'])
    assert parent is None


def test___repr___1():
    """
    When arguments are strings
    """
    assert (
        ParentMessage('a', 'b').__repr__()
        == "ParentMessage(channel='a', thread_ts='b')"
    )


def test___repr___2():
    """
    When channel is None
    """
    # Make sure no quotes around None value
    assert (
        ParentMessage(None, 'b').__repr__()
        == "ParentMessage(channel=None, thread_ts='b')"
    )


def test___repr___3():
    """
    When thread_ts is None
    """
    # Make sure no quotes around None value
    assert (
        ParentMessage('a', None).__repr__()
        == "ParentMessage(channel='a', thread_ts=None)"
    )


def test___repr___4():
    """
    When values are not strings or None
    """
    # This is an edge case, so we only check that it does not blow up
    ParentMessage([], 8934).__repr__()


def test_integration_of___str___and_from_string_1():
    """
    Happy Path
    """
    parent = ParentMessage('a', 'b')
    parent_str = str(parent)
    assert parent_str == 'a,b'
    reconstituted = ParentMessage.from_string(parent_str)
    assert reconstituted.channel == 'a'
    assert reconstituted.thread_ts == 'b'


def test_integration_of___str___and_from_string_2():
    """
    When channel is None

    Returns correct type
    """
    parent = ParentMessage(None, 'b')
    parent_str = str(parent)
    assert parent_str == 'None,b'  # Note this line would have failed in python < 3.2
    reconstituted = ParentMessage.from_string(parent_str)
    assert reconstituted.channel is None
    assert reconstituted.thread_ts == 'b'


def test_integration_of___str___and_from_string_3():
    """
    When thread_ts is None

    Returns correct type
    """
    parent = ParentMessage('a', None)
    parent_str = str(parent)
    assert parent_str == 'a,None'  # Note this line would have failed in python < 3.2
    reconstituted = ParentMessage.from_string(parent_str)
    assert reconstituted.channel == 'a'
    assert reconstituted.thread_ts is None
