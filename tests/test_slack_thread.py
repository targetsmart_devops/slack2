# ruff: noqa: S101
from unittest import TestCase
from unittest.mock import patch

from slack2.slack import Slack
from slack2.slack_thread import SlackThread


class TestSlackThread(TestCase):
    class SuccessRequest:
        EXAMPLE_TIMESTAMP = '1616523196.000200'

        @property
        def status_code(self):
            """
            Always returns 200
            """
            return 200

        def json(self):
            """
            Returns a minimum dictionary required in order for slack2 to find the ts
            """
            return {
                'ok': True,
                'channel': 'C01A8148LUX',
                'ts': '1730325766.848199',
            }

    def setUp(self):
        Slack.default_channel = None

    def test_three_in_thread(self):
        """
        When multiple messages sent, ensure that only one top-level message exists
        """
        Slack.default_channel = 'jack-test'

        with patch('requests.post') as mock_post:
            mock_post.return_value = self.SuccessRequest()
            SlackThread.send('one')
            SlackThread.send('two')
            SlackThread.send('three')

            call_one, call_two, call_three = mock_post.call_args_list
            # First message is a top-level message, so there is no thread_ts
            assert call_one[1]['json']['thread_ts'] is None
            assert call_two[1]['json']['thread_ts']

            # Second and third messages have thread_ts, and must match
            assert (
                call_two[1]['json']['thread_ts'] == call_three[1]['json']['thread_ts']
            )
