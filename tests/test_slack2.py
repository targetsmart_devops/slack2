"""
Tests for slack2 classes
"""

# ruff: noqa: D101, D102, D106, D205, S101, ANN201, SIM117, SLF001
# pragma pylint: disable=no-self-use, missing-class-docstring, protected-access
from __future__ import annotations

import logging
import ntpath
import re
import unittest
from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch

import pytest
import requests

from slack2 import ParentMessage, Slack, SlackWrap
from slack2.shared import InvalidTokenError
from slack2.util import Util

logging.basicConfig(file='log/test.log', level=logging.WARNING, datefmt='%H:%M:%S')


class TestParentMessage(TestCase):
    def test_channel(self):
        """
        Verify message returns correct channel
        """
        message = ParentMessage('one', 'two')
        assert message.channel == 'one'

    def test_thread_ts(self):
        """
        Verify message returns correct thread_ts
        """
        message = ParentMessage('one', 'two')
        assert message.thread_ts == 'two'

    def test___bool___1(self):
        """
        Verify a message with no channel is falsy
        """
        assert not ParentMessage(None, None)

    def test___bool___2(self):
        """
        Verify a message with a channel is truthy
        """
        assert ParentMessage('some-channel', None)


class TestUtil(TestCase):
    """
    This class contains a couple of useful tests toward making sure
    a python process could keep sending message through
    a slack api token rotation.

    Additional Thoughts:

    A. Token rotation is a two-step process.
       A1. Ask slack to generate a new token and assign it to the account
       A2. Load the new token into parameter store

    B. Ideally, slack messages sent after A1 but before A2
       do not raise any unhandled exceptions. That is,
       it's usually not the end of the world if a single slack
       message is dropped on the floor. But best if no unhandled exceptions
       occur.

    B. Doing an actual token rotation (after hours) seems like a viable option
       for verifying that the behavior is correct. After hours is preferable
       because of the time between A1 and A2. The test could go like this:
       B1. Run a script that sends a slack message every second for ten minutes to a test channel
       B2. Make sure that the script does not raise any unhandled exceptions in the
           interval between A1 and A2.
       B3. Make sure that after A2, slack messages are sent successfully.
       B4. Make sure that when testing that the delay between A1 and A2 is several
           slack messages longer than the maximum retry delay. This is to make
           sure that all test cases are encountered:
             1. fetch password and cache it
             2. use cached password successfully
             3. fetch password anew and retry when password is wrong
             4. continue long enough while unable to get a usable password that
                the retries eventually run out and the send fails.
                (Does this raise an unhandled exception?
                   Does this fail gracefully?
                   If so we might want to purposefully get the password and within ten seconds
                   update parameter store with the new value.)
             5. New request comes through that successfully fetches new password and uses it
               successfully

    C. You could manually test option B using a different account!
       Prior to (or in place of) doing an actual token rotation, you could do
       an actual token rotation using an alternate slack account and an
       alternate secret name in parameter store.  This would allow you to test
       the behavior before, during, and after the token is fully rotated.  This
       would also allow you to run the test multiple times without involving
       IT, as they manages the main slack instance.
    """

    def test_log_intermediate_failure_and_apply_side_effects_1(self):
        """
        Verify that when InvalidTokenError comes through,
        there is a call to cache_clear()
        """
        mock__api_token = Mock(wraps=Util._api_token)
        exc = InvalidTokenError('hello')
        with patch('slack2.util.Util._api_token', mock__api_token):
            Util.log_intermediate_failure_and_apply_side_effects(exc)
        mock__api_token.cache_clear.assert_called_once()

    def test_log_intermediate_failure_and_apply_side_effects_2(self):
        """
        Verify that InvalidTokenError is raised when
        invalid password is used
        """

        class InvalidAuthReturnValue:
            def json(self):
                return {
                    'ok': False,
                    'error': 'invalid_auth',
                    'warning': 'missing_charset',
                    'response_metadata': {'warnings': ['missing_charset']},
                }

        with patch(
            'requests.post',
            return_value=InvalidAuthReturnValue(),
        ), pytest.raises(InvalidTokenError):
            Slack._post(endpoint='any-endpoint', kwargs={})


class TestSlack2(TestCase):
    class SuccessRequest:
        EXAMPLE_TIMESTAMP = '1616523196.000200'

        @property
        def status_code(self):
            """
            Always returns 200
            """
            return 200

        def json(self):
            """
            Returns a minimum dictionary required in order for slack2 to find the ts
            """
            return {
                'ok': True,  # See Slack._send for explanation of "ok"
                'file': {
                    'shares': {
                        'private': {
                            'some-channel-id': [{'ts': self.EXAMPLE_TIMESTAMP}],
                        },
                    },
                },
            }

    def setUp(self):
        Slack.default_channel = None

    # Using a patch so the MagicMock goes away after the test
    @patch('requests.post', MagicMock(return_value=SuccessRequest()))
    def verify_sendfile(
        self,
        path_to_file,
        comment,
        channel=None,
        parent_message: ParentMessage | None = None,
    ):
        """
        Mocks requests.post in order to inspect the parameters used
        without actually using the network
        """
        parent_message = (
            Slack._empty_parent_message() if parent_message is None else parent_message
        )

        # This is the verbose version, but make sure it gets called
        # with no channel kwarg if not using one
        if channel is None:
            Slack.sendfile(path_to_file, comment, parent_message=parent_message)
        else:
            Slack.sendfile(
                path_to_file,
                comment,
                parent_message=parent_message,
                channel=channel,
            )

        basename = ntpath.basename(path_to_file)

        # pylint: disable=no-member
        assert requests.post.call_args[0] == ('https://slack.com/api/files.upload',)

        channel = parent_message.channel or channel or Slack.default_channel

        # pylint: disable=no-member
        params = requests.post.call_args[1]['params']
        assert params == {
            'token': Util.api_token(),
            'channels': [channel],
            'initial_comment': comment,
            'thread_ts': parent_message.thread_ts,
        }

        # pylint: disable=no-member
        assert requests.post.call_args[1]['timeout'] == Slack.TIMEOUT_SECONDS

        # The *files* portion is the reason we are making assertions
        # on each call_arg instead of using something like
        # assert_called_once_with()
        # pylint: disable=no-member
        files = requests.post.call_args[1]['files']
        assert files.keys() == {'file': 1}.keys()

        filename, handler, extension = files['file']
        assert filename == basename
        assert handler.name == basename
        assert extension == '.csv'

    def test__thread_ts_from_sendfile_req_1(self):
        """
        When no 'file' in req.json(), verify that this does not raise an exception
        """
        req = Mock()
        req.json.return_value = {'error': 'some-error'}
        output = Slack._thread_ts_from_sendfile_req(req)
        assert output is None

    def test__thread_ts_from_sendfile_req_2(self):
        """
        Verify that thread_ts is extracted
        """
        req = self.SuccessRequest()
        thread_ts = Slack._thread_ts_from_sendfile_req(req)
        assert thread_ts == self.SuccessRequest.EXAMPLE_TIMESTAMP

    def test_sendfile_1(self):
        """
        Verify that default channel is passed on to post()
        """
        Slack.default_channel = 'cherry'
        self.verify_sendfile('files/one.txt', 'charming')

    def test_sendfile_2(self):
        """
        Verify that custom channel is passed on to post()
        """
        self.verify_sendfile('files/one.txt', 'intelligent', channel='abc')

    def test_sendfile_3(self):
        """
        Verify that parent_message channel is passed on to post()
        """
        Slack.default_channel = 'kittens'
        self.verify_sendfile(
            'files/one.txt',
            'intelligent',
            parent_message=ParentMessage(
                'some-channel',
                self.SuccessRequest.EXAMPLE_TIMESTAMP,
            ),
        )

    def test_sendfile_4(self):
        """
        Raises an exception when no channel provided
        """
        with pytest.raises(Slack.NoChannelError):
            Slack.sendfile('files/one.txt', 'some words')

    # Using a patch so the MagicMock goes away after the test
    @patch('slack2.Util.api_token', MagicMock(return_value=None))
    def test_sendfile_5(self):
        """
        Verify that 503 error is logged when no bot token is available
        Verify that any exceptions are handled
        """
        with self.assertLogs() as context:
            Slack.sendfile('files/one.txt', 'intelligent prose', channel='b')
            assert (
                'ERROR:slack2:S3Logger: Slack API returned 503, Unable to send slack file with path "files/one.txt" and channel "b"'
                in context.output[0]
            )

    # USES NETWORK
    def test_sendfile_6(self):
        """
        Verify sending message returns a Parent Message object
        """
        channel = 'jack-test'
        Slack.default_channel = channel
        parent = Slack.sendfile('files/one.txt', 'intelligent prose')
        assert parent.channel == channel

        Slack.sendfile('files/one.txt', 'intelligent prose', parent_message=parent)

    # Using a patch so the MagicMock goes away after the test
    @patch('requests.post', MagicMock(return_value=SuccessRequest()))
    def test_sendfile_7(self):
        """
        When parent_message is None
        Does not blow up as long as default channel is set
        """
        Slack.default_channel = 'any'

        Slack.sendfile('pyproject.toml', 'some words', parent_message=None)

    # USES NETWORK
    def test_send_1(self):
        """
        Send message using the default channel
        Verify sending message returns a Parent Message object
        (It would be cleaner in one sense to make this two tests,
        but since this test uses network, combining)
        """
        channel = 'jack-test'
        Slack.default_channel = channel
        parent = Slack.send('testing the default channel')
        assert parent.channel == channel
        assert len(parent.thread_ts) > 1

    # USES NETWORK
    def test_send_2(self):
        """
        Send message using alternate channel
        """
        channel = 'jack-test2'
        parent = Slack.send('testing an alternate channel', channel=channel)
        assert parent.channel == channel
        assert len(parent.thread_ts) > 1

    # USES NETWORK
    def test_send_3(self):
        """
        Reply in Thread.
        Verifies that first message and its reply both return the same parent message
        """
        Slack.default_channel = 'jack-test'
        parent_1 = Slack.send('Parent message')
        parent_2 = Slack.send('First reply', parent_message=parent_1)
        assert parent_1 is parent_2

    def test_send_4(self):
        """
        Raises an exception when no channel provided
        """
        with pytest.raises(Slack.NoChannelError):
            Slack.send('anything')

    # Using a patch so the MagicMock goes away after the test
    @patch('slack2.Util.api_token', MagicMock(return_value=None))
    def test_send_5(self):
        """
        Verify that 503 error is logged when no bot token is available
        Verify that any exceptions are handled
        """
        with self.assertLogs() as context:
            Slack.send('a', channel='b')

            assert (
                "ERROR:slack2:S3Logger: Unable to fetch token from parameter store. Therefore did not Slack._send() with payload {'channel': 'b', 'text': 'a', 'thread_ts': None}"
                in context.output[0]
            )

    # Using a patch so the MagicMock goes away after the test
    @patch('requests.post', MagicMock(return_value=SuccessRequest()))
    def test_send_6(self):
        """
        When parent_message is None
        Does not blow up as long as default channel is set
        """
        Slack.default_channel = 'any'

        Slack.send('Hello', parent_message=None)

    def test_send_7(self):
        """
        Make sure that _send_token_reply() is called

        but only when parent_message=None, but mention is present.
        """
        Slack.default_channel = 'jack-test'

        a_parent = ParentMessage(thread_ts='some-timestamp', channel='some-channel')
        b_mention = 'some-userid'

        # data format:
        # { (some-parent, some-mention): whether_to_call_token_reply,
        #    ...
        # }
        data = {
            (None, None): False,
            (a_parent, None): False,
            # Only when parent is None and mention is present do we send a token reply
            (None, b_mention): True,
            (a_parent, b_mention): False,
        }

        for parent_and_mention, expect_reply in data.items():
            parent, mention = parent_and_mention
            print(f'parent: {parent}, mention: {mention}')
            with patch('requests.post') as mock_post, patch(
                'slack2.Slack._send_token_reply'
            ) as mock__send_token_reply:
                mock_post.return_value = self.SuccessRequest()
                Slack.send('Hello', parent_message=parent, mention=mention)
                if expect_reply:
                    assert mock__send_token_reply.called
                else:
                    assert not mock__send_token_reply.called

    @patch('slack2.Slack.send')
    def test_send_at_1(self, slack_send_mock):
        """
        Verify that send_at() calls send()

        """
        message = 'Hello'
        Slack.send_at(message)
        text = slack_send_mock.call_args[0][0]

        assert re.match(r'\d{2}:\d{2}(a|p)m.*', text)

        assert text.endswith(message)

    @patch('slack2.Slack.send')
    def test_send_at_2(self, slack_send_mock):
        """
        Verify that mention is passed to send()

        """
        message = 'Hello'
        mention = 'Ume'
        Slack.send_at(message, mention=mention)
        assert slack_send_mock.call_args[1]['mention'] == mention

    # Using a patch so the MagicMock goes away after the test
    @patch('requests.post', MagicMock(return_value=SuccessRequest()))
    def test_send_at_3(self):
        """
        When parent_message is None
        Does not blow up as long as default channel is set
        """
        Slack.default_channel = 'any'

        Slack.send_at('Hello', parent_message=None)

    def test__mention_1(self):
        """
        When mention is None
        """
        text = 'Hi'
        assert Slack._mention(text, None) == text

    def test__mention_2(self):
        """
        When mention is a string
        """
        text = 'Hi'
        mention = 'my_id'
        assert Slack._mention(text, mention) == '<@my_id> Hi'

    def test__mention_3(self):
        """
        When mention is a list
        """
        text = 'Hi'
        mention = ['your_id', 'my_id']
        assert Slack._mention(text, mention) == '<@your_id> <@my_id> Hi'

    @patch('slack2.Slack.send')
    def test_wrap_1(self, slack_mock_send):
        """
        Verify mention is passed along
        """
        Slack.default_channel = 'grapefruit'
        mention = 'Ume'
        with pytest.raises(AttributeError):
            with SlackWrap('testing the mention', mention=mention):
                'some_string'.unknown()  # pylint: disable=no-member

        # Slack.send gets called twice, and we only see the second call to send
        # the reply
        assert 'parent_message' in slack_mock_send.call_args[1]
        assert 'Traceback' in slack_mock_send.call_args[0][0]

    # USES NETWORK
    def test_update_1(self):
        """
        Post a message; then update its contents
        """
        Slack.default_channel = 'jack-test'
        parent = Slack.send('Original Content')
        status, error_message = Slack.update(
            'Updated Content',
            channel_id='C01A8148LUX',
            thread_ts=parent.thread_ts,
        )
        assert status == 200


if __name__ == '__main__':
    unittest.main()
