slack2
======

slack2 is a collection of Slack utilities for use at TS.

  - slack2.Slack.send: sends slack messages to arbitrary channels
  - slack2.Slack.send_at: same as send(), but includes timestamp
  - slack2.Slack.sendfile: Uploads file to channel
  - slack2.SlackWrap: A context manager that sends notification on exceptions


There is already a slack app configured for your use. (See Prerequisites)
The bot token for this app is stored in AWS Parameter Store. Since
all TS environments have access to AWS Parameter Store, the bot token
is fetched dynamically as needed.



Installation
------------

Note as of July 2023, slack2 is stored in (and unadvertised) public git repo.
This means ssh keys are not required to install it.

    pip install git+ssh://git@bitbucket.org/targetsmart_devops/slack2


IAM Policy
----------

If the environment is not already able to read Parameter Store and write to S3,
you may consider adding the policy named `slack2-policy`.



Prerequisites: Add Slack App to Your Channels
----------------------------------------------

There is already a slack app configured called early_vote_notifier.

You must add this app to all of the channels you want to use it with.
This gives the app permission to post there.

  - Click the channel in slack
  - Click *Add an App*
    (In a new channel it's immediately available)
    (In mature channels, click *info* in the channel heading, then *more*, then *add an app*)


### Use a Different App

See *Setting Up a Slack App From Scratch*


Sending Messages (Slack.send)
-----------------------------

### Using the Default Channel

    from slack2 import Slack

    Slack.default_channel = 'blue-team'
    Slack.send('Message to Default Channel')
    Slack.send('Another message')
    Slack.send('Here is <http://example.com|a link>', escape=False)

### Using a Custom Channel

    Slack.send('Message to Custom Channel', channel='red-team')


### Mentioning One or more Users

See *Notes on Mentioning Users*

    Slack.send('Message mentioning Patrick', mention='UH5UVJUQ3')
    Slack.send('Message mentioning Jack & Jill', mention=['UH5UVJUQ4', 'UH5UVJUQ5'])


### Replying In Thread

See *Notes on Replying In Thread*

    parent = Slack.send("Let's talk about Code Review")
    Slack.send('Additional thoughts ...', parent_message=parent)


### ALWAYS Replying in Thread

If you have a python process where you want ALL slack messages to be in a single thread, use the SlackThread class

    from slack2 import Slack, SlackThread

    Slack.default_channel='my-channel'

    SlackThread.send('First')
    SlackThread.send('Second')
    SlackThread.send('Third...a cool <https://example.com|link>', mention='UAK5330493', escape=False)

Note you can even mix and match Slack / SlackThread

    Slack.send('A top level message')
    SlackThread.send('Fourth')


Sending Message with Timestamp (Slack.send_at)
----------------------------------------------

If the current time happens to be 12:52pm, then these two lines do the same thing:

    Slack.send('12:52pm Hello')
    Slack.send_at('Hello')


(A better name for this method would be `Slack.send_with_timestamp`)

Slack.send_at has the same signature as Slack.send.


Updating Messages
-----------------

You can update a previously sent message, even if that message included
an uploaded file.

Note you must provide a channel_id, not a channel name.
Note `mention` and `escape` keyword arguments may also be used.

    Slack.default_channel = 'some-channel'
    parent_msg = Slack.send('Initial Content')
    Slack.update('Updated Content', 'id-of-channel-some-channel', parent_msg.thread_ts)


Uploading Files (Slack.sendfile)
--------------------------------

### Using the Default Channel

    Slack.sendfile('path/to/file',
                   '*Some Introductory Text*\n\nHere is the file:')


### To an Alternate Channel

    Slack.sendfile('path/to/file',
                   '*Some Introductory Text*\n\nHere is the file:',
                   channel='my-alternate-channel')

### Mentioning One or More Users

See *Notes on Mentioning Users*

    # Mention one user
    Slack.sendfile('path/to/file',
                   '*Some Introductory Text*\n\nHere is the file:',
                   mention='UH5UVJUQ3'
                   )


    # Mention multiple Users
    Slack.sendfile('path/to/file',
                   '*Some Introductory Text*\n\nHere is the file:',
                   mention=['UH5UVJUQ3','UH5UVJUQ4']
                   )

### Replying In Thread

See *Notes on Replying In Thread*

    parent = Slack.sendfile('path/to/file',
                   '*Some Introductory Text*\n\nHere is the file:',
                   )
    Slack.sendfile('path/to/file',
                   'Followup Thoughts with another file:',
                   parent_message=parent,
                   )

Markdown
--------

Markdown is supported by default. If you want to use markdown-style links
(or anything that requires <,>,| characters to be parsed by Slack),
supply the keyword argument `escape=False`.

    Slack.send('Here is <http://example.com|a link>', escape=False)


Exceptions Raised by slack2
---------------------------

Since version 2.5.2, slack2 is not expected to raise any exceptions.

### Parameter Store

When fetching data from parameter store, any exception is handled and logged.

### Slack API

When posting to the slack API using `requests.post()`, exceptions inherited from
requests.exceptions.RequestException
w
requests.exceptions is retried using tenacity,
but in the case of continued failure, logs the error (Both to the python logger
and to S3) but does not raise an exception.


Notify on Exceptions (SlackWrap)
--------------------------------

SlackWrap is a context manager. If you wrap your code in SlackWrap,
you will get a notification if an unhandled exception occurs.
Optionally, you can also be notified when the interpreter enters and exits
the context manager.

### Notify on Exception Only

The base functionality of SlackWrap is to notify on exceptions.

    from slack2 import Slack, SlackWrap
    Slack.default_channel = 'orange-team'

    with SlackWrap('project_apple'):
        do_something()

### Notify on Start, Stop, and Exception

When the `verbose` flag is `True`, it also notifies when entering and exiting the context manager.

    with SlackWrap('project_apple', verbose=True):
        do_something()


### Mention One or More Users

See *Notes on Mentioning Users*

    with SlackWrap('project_glue', mention='UH5UVJUQ3'):
        do_something()

    with SlackWrap('project_glue', mention=['UH5UVJUQ3', 'UH5UVJUQ4']):
        do_something_else()

(You may also use the `mention` keyword when `verbose` is True,
but be aware that the `mention` part will only affect the exception notification,
not the entering and exiting of the context manager.)


Notes on Mentioning Users
-------------------------
*Mention* is when you put someone's username in a message. In the message or
file upload it will show up as `@[username]`, but to use this feature you
must actually their user ID.

Note user ids start with "U".

To find a user id with the Slack GUI, you can follow these steps:

1. Click on the user's profile picture to access the pane that displays their Profile
1. Click the `⋮` symbol to access a sub-menu to then select "Copy member Id"

Or to get a list of all users and their ids for your organization, you can use this API endpoint:

https://api.slack.com/methods/users.list/test


Notes on Default Channel
------------------------

If you do not specify a default_channel, you must pass `channel` as a keyword arg

    from slack2 import Slack
    Slack.send('Hello', 'some-channel')

If you do specify a default_channel, you may omit passing in
a channel:

    from slack2 import Slack
    Slack.default_channel = 'my-default'
    Slack.send('Hello')



Notes on Channel vs Channel ID
------------------------------

When using the *channel* keyword in Slack.send(), Slack.sendfile(), etc,
*channel* can point to any of the following:

  - channel name with leading pound sign: #my-alternate-channel
  - channel name without leading pound sign: my-alternate-channel
  - channel id: ZA2BD7923


Notes on Best Effort Sending
----------------------------

What happens if Slack (Or AWS Parameter Store) are down?
Slack2 does uses the retrying package to retry the send, but it doesn't
retry forever. (See slack2.Slack.KWARGS for details.)

When slack2 retries, it calls logging.info(). If the retries are exhausted
without a successful send, it calls logging.error().


Notes on Replying In Thread
---------------------------

The `parent_message` keyword arg can be set on any of these methods:

    Slack.send
    Slack.send_at
    Slack.sendfile

### Mix & Match

You can start a thread with any of the above methods, and then use any of the above
methods to reply:

    parent_1 = Slack.send_at('Hello')
    Slack.sendfile('/path/to/file', 'Introductory Words', parent_message=parent_1)

    parent_2 = Slack.sendfile('/path/to/file', 'Introductory Words')
    Slack.send('Definitely!', parent_message=parent_2)

### Channel not Required for Reply

Note that the channel keyword argument is ignored when parent_message is present.
This is to prevent the confusion caused by accidentally setting the wrong channel
when replying in a thread.

### Chaining Replies

Note that in the following example, parent_1 and parent_2 are the same object.
This is because replies are always to the same parent, not to children.

    parent_1 = Slack.send('first')
    parent_2 = Slack.send('second', parent_message = parent_1)

    parent_1 is parent_2 # True

### Setting explicit `None` parent_message

As of version 2.6.1 you can pass in `None` for parent message
and get the same effect as not setting parent_message at all.

Note you must either supply a channel or have already set `Slack.default_channel`

    Slack.send('hello', channel='some-channel', parent_message=None)


Notes on Bot Token Configuration
--------------------------------

slack2 will automatically access the bot token from AWS parameter store


Setting Up a Slack App From Scratch
-----------------------------------

(TargetSmart employees may skip this section since they all share single slack app.)

slack2 requires a *bot token*, not a webhook url.

The advantage of using a bot token is that then your bot is able to
post to arbitrary channels (as long as it has permission),
while keeping your slack secrets out of your source code.

### Bot token

From the app page (api.slack.com/apps/your-slack-app-id), in the left menu click
"Oauth & Permissions" to view the bot token, which starts in `xoxb-`


### Scopes

Your slack app needs the following scopes:

  - files:write (for file uploads)
  - chat:write (for posting plain text into arbitrary channels)

Here are the basic steps (as of Nov 2020) for adding scopes:

  - Start at https://api.slack.com/apps
  - Click your app.
  - Click Add Features and Functionality -> Permissions
  - Find the scopes you need and add them

We actually have the following scopes added to the ts_notifier app.
Some of these are to support scraping the #ask_a_question channel.
  channels:history
  channels:manage
  chat:write
  files:read
  files:write
  groups:history
  incoming-webhook
  users:read

Slack now allows you to view/edit a manifest showing all your scopes.
See doc/manifest.json in this repo for a copy of the current scopes.

### Add App to Channels

You must add your slack app to all of the channels you want to use it with.
This gives the app permission to post there.

  - Click the channel in slack
  - Click *Add an App*
    (In a new channel it's immediately available)
    (In mature channels, click *info* in the channel heading, then *more*, then *add an app*)


Rotating your Bot Token
-----------------------

As of Nov 2023, there appears to be no clean way to rotate
your bot token for a different one. (Unless you go all-in
and sign up for automatic bot-token rotation.)

What is offered is "Revoke All OAUTH Tokens".
To find that option, click "Oauth & Permissions" on the left
and then scroll all the way to the bottom. Note the downside is you
will need to re-invite your bot into all channels it uses.

Here are the steps:

1. Press "Revoke All OAUTH tokens"

2. Press "Reinstall to Workspace", which will generate a new bot token.

3. Store your new bot token in parameter store.

4. Invite your bot back into all the channels that it uses.

### Error Messages During Token Rotation

Note this was tested with an alternate slack account on Nov 6, 2023.
A script was set up to send a slack message every second. The script kept running,
even through getting the above-mentioned error messages and exceptions.
And once all steps were complete, the same script started posting messages again.

Here are the error messages returned during each step of the process:

A. After "Revoke All OAUTH Tokens" you will get error message "account_inactive".

B. After "Reinstall to Workspace" slack2 will raise InvalidTokenError and will retry.

C. After Adding the new token to parameter store, you will get error message "channel_not_found".

D. After Adding the bot to the channel in question, it will send new messages successfully.



Development
-----------

    pip install boto3 requests retrying pytest pylint black


Running Tests
-------------

(You will need AWS credentials in order to run tests locally.)

    env/bin/pip install pytest
    env/bin/pytest test_slack2.py


### Filtered tests

    env/bin/pytest test_slack2.py -k <filter_string>

### Integration tests

The *main* block of slack2.py does an end-to-end verification of the main options.
You must provide configuration.

    env/bin/python slack2.py

Tox
---

Tox allows you to run tests for multiple python interpreters.
slack2 is tested against the interpreters listed in tox.ini.

    python -m tox

Or in parallel:

    python -m tox -p

Or just one interpreter

    python -m tox -e py38


Testing Caveats with Retrying
-----------------------------

Testing can be squirrely when the retrying package is in place.

When running slack2 normally, it is set up to call logging.info any
time an exception is being retried via the retrying package. But when
running pytest, log messages (WARNING and higher) are only shown
after a test runs and fails.  So when running the test suite,
you may see it hanging.

Here are a couple things you can do to find out what's hanging:

  - Run `python slack2.py`. If something is failing and getting retried,
    you will see the log message to STDOUT.
  - Call pytest with the `-v` option so it shows which test is running.
    Then you can add a breakpoint as necessary
  - Comment out the @retry lines in slack2.py and run the tests again
    (remember to uncomment before making any commits)



Ideas for Future Development
----------------------------

- Retry if slack API provides a non-200 error code.
  (Currently the only retries are if requests.post() raises an exception, such as TimeoutError)
- Retry for longer amounts of time if user provides max_wait timeout.
- Get the threaded version working so that even if the slack api is slow, the response
  for the user is fast.
- From the Slack.send() classmethod, instantiate an object so you don't have to pass so many variables
  from one method to the next.
- Look into using https://github.com/kislyuk/watchtower as a replacement for S3Logger
  "One idea to potentially explore would be to simplify the codebase by using the watchtower
  "library which provides a log handler for centralized logging to AWS CloudWatch.
  "You could then remove the S3 logging code and get the benefits of CloudWatch logging (search/review in console, etc)." -- Ben
- Disable the S3Logger when the DISABLE_S3_LOGGER environment variable is set
  (So that running tests and example from localhost does not write to the bucket)
