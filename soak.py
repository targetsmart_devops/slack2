"""
This module is intended to repeatedly send notifications and files
"""

from slack2 import Slack

Slack.default_channel = 'jack-test'

counter = 0
while True:
    msg = f'Soak {counter}'
    parent = Slack.send(msg)
    for i in range(100):
        Slack.send(f'{msg} ({i})', parent_message=parent)
    for j in range(100):
        Slack.sendfile('files/one.txt', f'Soak file {j}', parent_message=parent)
    counter += 1
