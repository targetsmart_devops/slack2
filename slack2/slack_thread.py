from __future__ import annotations

import contextlib
import logging
from threading import RLock
from typing import TYPE_CHECKING

from slack2 import Slack
from slack2.shared import LOGGER

if TYPE_CHECKING:
    from typing import Iterable

    from slack.parent_message import ParentMessage


class SlackThread:
    """
    Send all slack messages in a single thread

    Does not require you to pass in a parent_message kwarg
    """

    __CLASS_PARENT_MESSAGE = None
    RLOCK = RLock()

    @classmethod
    def send(
        cls,
        text: str,
        *,
        channel: str | None = None,
        mention: str | Iterable | None = None,
        escape: bool = True,
    ) -> None:
        """
        Send a message.

        All messages sent with this method will be in the same slack thread

        Arguments:
        text:    Words to send

        channel: The channel where message will be sent.
                 Only required in the case where both of these are true:
                 1. When Slack.default_channel has not been set, and
                 2. When it is the first message to be sent through SlackThread
                    since the python process began

        mention: (Optional)
                 A (str) user_id or list of (str) user_ids to mention
                 using "@So and So"
                 User id is a nine-character string beginning with 'U'.
                 User ids can be found from the users.list endpoint
                 or by right-clicking on a person's name in a message
                 and selecting "Copy Link".

        escape:  If True, convert <,>,| to their html escaped counterparts
                 (Set escape=False if you want to use markdown-style links)
        """
        # Acquire up front so that only one message sends at a time until we have a parent message
        # This avoids the race condition where two first messages sent simultaneously both end up as top-level-messages
        cls.RLOCK.acquire()
        try:
            existing_parent = cls._existing_parent_message()
            if existing_parent:
                # Release the lock becase the only reasons to lock are:
                # 1. Wait for a __PARENT_MESSAGE to be present
                # 2. Make sure all threads read the same value of __PARENT_MESSAGE
                cls.RLOCK.release()

            new_parent = Slack.send(
                text,
                channel=channel,
                mention=mention,
                parent_message=existing_parent,
                escape=escape,
            )
            if not existing_parent:
                cls._set_class_parent_message(new_parent)

        finally:
            # RuntimeError is raised if the lock was released early; this is normal
            with contextlib.suppress(RuntimeError):
                cls.RLOCK.release()

    @classmethod
    def _set_class_parent_message(cls, new_parent: ParentMessage):
        with cls.RLOCK:
            cls.__CLASS_PARENT_MESSAGE = new_parent

    @classmethod
    def _existing_parent_message(cls) -> ParentMessage | None:
        with cls.RLOCK:
            return cls.__CLASS_PARENT_MESSAGE


if __name__ == '__main__':
    from threading import Thread

    Slack.default_channel = 'jack-test'

    # Sending from multiple threads simultaneously
    # Verify that only one of these ends up as a top-level message
    def _test_send(text):
        SlackThread.send(text)

    threads = []

    for i in range(5):
        threads.append(
            Thread(target=_test_send, args=(f'{i}. Hello multiple threads',))
        )
    for th in threads:
        th.start()
    for th in threads:
        th.join()
