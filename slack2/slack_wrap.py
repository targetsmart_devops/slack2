"""
Module for sending slack notifications, uploading files to slack,
and notifying of exceptions via slack
"""

from __future__ import annotations

import json
import logging
import traceback

from slack2.slack import Slack


class SlackWrap:
    """
    A context manager that sends an exception notification.
    Note: Slack.default_channel must be set before invoking

    :param str descriptor: Descriptor that gets sent with exception notification
    :param bool verbose:   If True, notifies via slack when context manager
                           enters and when it exits
    :param mention:        A (str) user_id or list of (str) user_ids to mention
                           using "@So and So"
                           User id is a nine-character string beginning with 'U'.
                           User ids can (only?) be found using the
                           users.list endpoint


    >>> Slack.default_channel = 'channel-5'
    >>> with SlackWrap('my_project'):
    ...     do_something()

    >>> with SlackWrap('my_project', mention='U789BAC32'):
    ...     do_something()


    """

    __slots__ = 'descriptor', 'mention', 'verbose'

    def __init__(self, descriptor, verbose=False, mention=None):
        self.descriptor = descriptor
        self.verbose = verbose
        self.mention = mention

    def __enter__(self):
        if Slack.default_channel is None:
            raise Slack.NoChannelError
        if self.verbose:
            Slack.started(self.descriptor)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type is not None:
            # Write detailed informtion (including stack trace) to the error log
            # Note this does NOT use S3Logger because this error will be case-specific
            # and is unlikely to be cause by the slack2 library or the slack API
            logging.error('ERROR: SlackWrap caught the following error:', exc_info=True)

            # Send basic information about the error to slack
            msg = f'FAILED to complete {self.descriptor} ({exc_value})'
            parent = Slack.send(msg, mention=self.mention)

            # Send exception details as reply so the developer can see them,
            # but without clogging the main channel
            details = traceback.format_exception(exc_type, exc_value, exc_traceback)
            details_str = '\n'.join(details)
            Slack.send(f'```{details_str}```', parent_message=parent)
            return

        if self.verbose:
            Slack.completed(self.descriptor)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, datefmt='%H:%M:%S')

    Slack.default_channel = 'jack-test'

    # Happy Path
    with SlackWrap(__file__, verbose=True):
        print('inside the context')

    # Test exception message
    with SlackWrap(__file__, mention='U0181U8BJ93'):
        json.undefined_method()  # pylint: disable=no-member
