from __future__ import annotations

import logging

# Specify an explicitly named logger so log messages will be prefaced with "slack2"
LOGGER = logging.getLogger('slack2')


class InvalidTokenError(Exception):
    """
    Raised when we use the wrong token when talking to the slack API
    """
