from __future__ import annotations

COMMA = ','


class ParentMessage:
    """
    This class allows you to store channel and thread_ts together
    in a read-only object.

    (Originally implemented with dataclasses, but dataclasses
    begins with python3.7 (3.6 if you `pip install dataclasses`)
    """

    __slots__ = ('__channel', '__thread_ts')

    def __init__(self, channel, thread_ts):
        self.__channel = channel
        self.__thread_ts = thread_ts

    @property
    def channel(self):
        return self.__channel

    @property
    def thread_ts(self):
        return self.__thread_ts

    def __bool__(self):
        """
        The truthiness of a ParentMessage is used to determine whether
        it is an empty parent message or not
        """
        return bool(self.channel)

    def __repr__(self) -> str:
        """
        Return a visual representation
        """
        channel_formatted = (
            f"'{self.channel}'" if isinstance(self.channel, str) else self.channel
        )
        thread_ts_formatted = (
            f"'{self.thread_ts}'" if isinstance(self.thread_ts, str) else self.thread_ts
        )
        return f'ParentMessage(channel={channel_formatted}, thread_ts={thread_ts_formatted})'

    def __str__(self) -> str:
        """
        Return a concise string-representation

        This is used by Nagios to save state between alerts.
        """
        return f'{self.__channel}{COMMA}{self.__thread_ts}'

    @classmethod
    def from_string(cls, text):
        """
        Build a ParentMessage from the __str__ representation

        This is useful when reconstituting a parent message
        that has been stored in string form in a database

        >>> ParentMessage('some-channel,some-thread-ts')
        ParentMessage(channel='some-channel', thread_ts='some-thread-ts')
        """
        # If the data type and comma count are correct, return a ParentMessage
        # containing the expected values
        if isinstance(text, str) and (text.count(COMMA) == 1):
            channel, thread_ts = text.split(COMMA)

            # Convert str(None) back to None so that we don't try to use string "None"
            # as a channel name or as a thread_ts
            #
            # Note this creates an edge case bug such that if there happens to be
            # a channel named "None", you will not be able to use
            # slack2 to reply-in-thread in that channel.
            channel_to_use = None if channel == str(None) else channel
            thread_ts_to_use = None if thread_ts == str(None) else thread_ts
            return cls(channel_to_use, thread_ts_to_use)

        # Otherwise return None
        # Note that when parent_message is None, the message goes to the
        # channel instead of as a reply to a thread.
        return None
