"""
Module for sending slack notifications, uploading files to slack,
and notifying of exceptions via slack
"""

from __future__ import annotations

import functools

import boto3
import requests

from slack2.s3_logger import S3Logger
from slack2.shared import InvalidTokenError


class ParameterStoreError(Exception):
    """
    Raised when there is an error fetching from parameter store
    """


class Util:
    """
    A utility class for methods used by this module.
    """

    # Setting region explicitly to ensure that this runs on all boxes equally,
    # whether or not AWS_DEFAULT_REGION environment variable is set
    REGION = 'us-east-1'

    TOKEN_PARAMETER_NAME = '/slack/bot_token'

    # pragma pylint: disable=too-few-public-methods
    @classmethod
    def log_intermediate_failure_and_apply_side_effects(cls, exc):
        """
        This method does three things:

        1. Log exception before each retry so we can see that it is being retried.

        2. Apply side effects

        3. Also return a boolean to indicate whether to retry or not.

        WHY THIS IS IN A SEPARATE CLASS
        The Util class is defined before the Slack class is defined.
        That way Slack.KWARGS has a way to reference Util.log_intermediate_failure_and_apply_side_effects

        WHICH EXCEPTIONS TO RETRY
        See block below for which exception types are retried

        :return: :bool:
        """
        exc_message = ''
        if exc.args:
            exc_message = f'({exc.args[0]})'

        name = exc.__class__.__name__
        warn_msg = (f'slack2: Intermediate failure. Will retry {name}: {exc_message}',)

        if isinstance(exc, requests.exceptions.RequestException):
            # We retry requests exceptions because we expect them to be
            # ephemeral network failures
            # See https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
            S3Logger.warning(warn_msg)
            return True

        # Only need to retry InvalidTokenError once...
        # tenacity is currently set to retry 2 or 3 times,
        # which is probably fine
        if isinstance(exc, InvalidTokenError):
            # Apply a side effect that invalidates the api_token cache
            # so that when this error is retried,
            # it will pull the token anew from parameter store
            # instead of using the one in the cache
            # (You are expected to only land here when the token is ever rotated)
            cls._api_token.cache_clear()
            S3Logger.warning(warn_msg)
            return True

        S3Logger.error(f'slack2: Atomic failure (no retries for {name}): {exc_message}')
        return False

    @classmethod
    def log_final_failure(cls, retry_state):
        """
        Only ends up here if:
            - Failed at least once
            - Never succeeded
        """
        msg = f'Retried but never succeeded. Final failure {retry_state}'
        S3Logger.error(msg)

    @classmethod
    def api_token(cls):
        """
        Returns a cached copy of slack API token from AWS Parameter Store

        If the cache is empty or has been cleared, it first fetches a new copy
        and populate the cache.

        :return: :str: or :None:

        Notes:
            Returns the decrypted value from parameter store

            NOT SURE if it makes sense to retry this method, since boto3
            already has retry operations in place.
        """
        try:
            return cls._api_token()
        except ParameterStoreError:
            # Return None so that caller knows we do not have a token
            return None

    @classmethod
    # Note that lru_cache in python 3.7 and older requires the `maxsize` argument
    # to be present.
    @functools.lru_cache(maxsize=1)
    def _api_token(cls) -> str:
        # creating a client at runtime so code is pickleable for prefect
        client = boto3.client('ssm', region_name=cls.REGION)

        # XXX Temporarily adding this print statement so we can see cache misses
        print('Fetching slack2 token from parameter store')
        try:
            # Attempts were made to set connection timeouts on boto3,
            # but for example if you are on localhost and you turn off your
            # wifi adapter, it still hangs for 20 seconds regardless of
            # timeout options I've tried.
            param = client.get_parameter(
                Name=cls.TOKEN_PARAMETER_NAME,
                WithDecryption=True,
            )
        except Exception as exc:  # pylint: disable=broad-except
            S3Logger.error(f'Parameter Store: Unable to fetch token: {exc}')
            # Raise an exception so that the response from this method is not cached
            raise ParameterStoreError from exc

        return param['Parameter'].get('Value')
