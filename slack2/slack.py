"""
Module for sending slack notifications, uploading files to slack,
and notifying of exceptions via slack
"""

# ruff: noqa: E501
# Python 3.7 or greater is required to use the type annotations used in this file.
# (setup.py restricts to versions that pass tox tests)
from __future__ import annotations

import logging
import ntpath
import os
from datetime import datetime
from types import MappingProxyType
from typing import TYPE_CHECKING

import requests
from dateutil import tz
from tenacity import (
    RetryError,
    Retrying,
    retry_if_exception,
    stop_after_delay,
    wait_exponential,
)

from slack2.parent_message import ParentMessage
from slack2.s3_logger import S3Logger
from slack2.shared import InvalidTokenError, LOGGER
from slack2.util import Util

EASTERN = tz.gettz('America/New_York')
TOKEN_REPLY_MESSAGE = ':point_up:'  # noqa: S105

if TYPE_CHECKING:
    from typing import Iterable

    from requests import Response


class Slack:
    """
    Class for sending slack notifications and uploading files to slack

    RETRYING HTTP CALLS
    This class does use the retrying package for the methods that
    make HTTP calls. But if it runs out of retries, it handles the exception.

    This means that if you call:

        Slack.default_channel = 'my-channel'
        Slack.send('Something Real')
        logging.info('After Message')

    and the Slack servers happen to be down that day,
    the "After Message" will still be printed.
    """

    class NoChannelError(Exception):
        """
        Raised when no channel is specified
        """

    class ChannelIdError(Exception):
        """
        Raised when channel ID appears to not be a channel ID
        """

    SENDFILE_ENDPOINT = 'https://slack.com/api/files.upload'
    CHAT_POSTMESSAGE_ENDPOINT = 'https://slack.com/api/chat.postMessage'
    CHAT_UPDATEMESSAGE_ENDPOINT = 'https://slack.com/api/chat.update'

    TIMEOUT_SECONDS = 10

    SPACE = ' '
    SIMPLE_TIMESTAMP_FORMAT = '%I:%M%p'

    KWARGS = MappingProxyType(
        dict(
            wait=wait_exponential(
                multiplier=1,
                min=4,
                max=10,
            ),  # Retry exp (indefinitely)
            stop=stop_after_delay(25),  # Stop after 25 seconds total
            retry=retry_if_exception(
                Util.log_intermediate_failure_and_apply_side_effects,
            ),  # Which exceptions to retry
            retry_error_callback=Util.log_final_failure,  # After all failures, log
            reraise=True,  # Put the exception of interest at the tail end of the stack trace
        ),
    )

    default_channel = None

    @classmethod
    def send(
        cls,
        text: str,
        *,
        channel: str | None = None,
        mention: str | Iterable | None = None,
        parent_message: ParentMessage | None = None,
        escape: bool = True,
    ) -> ParentMessage | None:
        """
        Send a message

        text:            Message to send

        channel:         The channel where message will be sent.
                         Note channel is optional if Slack.default_channel
                         has already been set
                         Note channel is ignored if parent_message is set.

        mention:         (Optional)
                         A (str) user_id or list of (str) user_ids to mention
                         using "@So and So"
                         User id is a nine-character string beginning with 'U'.
                         User ids can be found from the users.list endpoint
                         or by right-clicking on a person's name in a message

        parent_message:  (Optional)
                         If present, send message as a reply to the parent message.
                         (Start or continue a thread)

        escape:          If True, convert <,>,| to their html escaped counterparts
                         (Set escape=False if you want to use markdown-style links)


        >>> Slack.send('hi', channel='#my-channel')
        >>> Slack.send('hi', channel='my-channel')
        >>> Slack.send('hi', channel='C3155EZJB')
        >>> Slack.send('hi', mention='UBZ1359K')
        >>> parent = Slack.send('First Message')
        >>> Slack.send('Replying in thread', parent_message=parent)
        """
        # Allow user to pass `None` for parent message, without breaking when we access parent message attrubutes
        parent_message = (
            cls._empty_parent_message() if parent_message is None else parent_message
        )

        channel = parent_message.channel or channel or cls.default_channel
        if channel is None:
            raise cls.NoChannelError

        # Convert text to a string just in case it comes in as a dictionary
        text = str(text)

        # Add environment
        # text = f'[{settings.ENVIRONMENT}] {text}'

        # Escape certain characters
        if escape:
            text = cls._escape(text)

        # Add mentions
        text = cls._mention(text, mention)

        payload = dict(channel=channel, text=text, thread_ts=parent_message.thread_ts)

        status, thread_ts = cls._send(payload)

        if status != 200:
            # In the case of error, we return the best parent message we can
            return ParentMessage(channel, None)

        # return original parent_message if present and truthy
        if parent_message:
            # This is already in a thread so guaranteed to show up in "Threads" view
            return parent_message

        new_parent_message = ParentMessage(channel, thread_ts)

        # Ensure that any mentions show up in slack "Threads" view
        # For messages with mention but no parent_message, this requires a reply of some sort
        if mention:
            cls._send_token_reply(parent_message=new_parent_message)

        return new_parent_message

    @classmethod
    def update(
        cls,
        text: str,
        *,
        channel_id: str,
        thread_ts: str,
        mention: Iterable | str | None = None,
        escape: bool = True,
    ) -> tuple:
        """
        Update contents of a message

        :param: str :text:       Words to send

        :param str thread_ts:    Which message to update

        :param: str :channel_id: The channel ID where message will be sent.
                                 (Channel name will not work, unfortunately)
                                 (Note this is required even if Slack.default_channel is set)

        :param str mention:       (Optional)
                                  A (str) user_id or list of (str) user_ids to mention
                                  using "@So and So"
                                  User id is a nine-character string beginning with 'U'.
                                  User ids can (only?) be found using the
                                  users.list endpoint

        :param: bool escape:      If True, convert <,>,| to their html escaped counterparts
                                  (Set escape=False if you want to use markdown-style links)

        :return: None

        >>> parent = Slack.send('hi')
        >>> Slack.update('Well Hello There', channel_id='C01A8148LUX', thread_ts = parent.thread_ts)
        """
        channel_id = str(channel_id)
        if not channel_id:
            msg = 'must supply a channel_id'
            raise cls.ChannelIdError(msg)

        if channel_id[0] not in ('C', 'D', 'G'):
            # This check is here to make sure the developer is passing in
            # an actual channel_id
            # See https://api.slack.com/apis/conversations-api#shared_channels
            msg = 'channel_id must start with C, D, or G'
            raise cls.ChannelIdError(msg)

        # Convert text to a string just in case it comes in as a dictionary
        text = str(text)

        # Escape certain characters
        if escape:
            text = cls._escape(text)

        # Add mentions
        text = cls._mention(text, mention)

        payload = dict(channel=channel_id, text=text, ts=thread_ts)

        status, error_message = cls._update(payload)

        if status != 200:
            # If status is None, we did not get a response from the slack API
            msg = f'Slack API returned {error_message}, Unable to update slack message with content "{text}" and channel_id "{channel_id}"'
            S3Logger.error(msg)
        return status, error_message

    @classmethod
    def _update(cls, payload):
        """
        Actual post happens here, with retrying

        :param dict payload:
        :return: :tuple:
        """
        # With the CHAT_POSTMESSAGE_ENDPOINT, token must be
        # in header wth special formatting
        token = Util.api_token()
        if token is None:
            # 503 Service is temporarily unavailable
            # You will end up here if there is an error fetching the token
            # Check logs to see exception type
            return 503, 'Service Unavailable'

        headers = dict(Authorization=f'Bearer {token}')

        req = cls._post_with_retries(
            cls.CHAT_UPDATEMESSAGE_ENDPOINT,
            json=payload,
            headers=headers,
        )

        if req is None:
            return None, None

        # Unfortunately, if you pass the wrong channel, the request returns 200
        # but it also includes an error
        error_message = req.json().get('error')
        if error_message:
            return 400, error_message

        return req.status_code, None

    @classmethod
    def _mention(cls, text: str, id_or_ids: str | Iterable) -> str:
        ids = id_or_ids
        if ids is None:
            return text

        if isinstance(ids, str):
            ids = [ids]

        mentions = [f'<@{id_}>' for id_ in ids]
        prepend = cls.SPACE.join(mentions)
        return f'{prepend} {text}'

    @classmethod
    def send_at(
        cls,
        text: str,
        *,
        channel: str | None = None,
        mention: str | Iterable | None = None,
        parent_message: ParentMessage | None = None,
        escape: bool = True,
    ):
        """
        Sends a message prepended with a simple timestamp
        Args are the same as for send()
        """
        tstamp = datetime.now(EASTERN).strftime(cls.SIMPLE_TIMESTAMP_FORMAT).lower()
        text = f'{tstamp} {text}'
        return cls.send(
            text,
            channel=channel,
            mention=mention,
            parent_message=parent_message,
            escape=escape,
        )

    @classmethod
    def _send(cls, payload: dict) -> tuple:
        """
        Actual post happens here, with retrying

        :param dict payload:
        :return: :tuple:
        """
        token = Util.api_token()
        if token is None:
            # 503 Service is temporarily unavailable
            # You will end up here if there is an error fetching the token
            # Check logs to see exception type
            msg = f'Unable to fetch token from parameter store. Therefore did not Slack._send() with payload {payload}'
            S3Logger.error(msg)
            return 503, None

        # With the CHAT_POSTMESSAGE_ENDPOINT, token must be
        # in header wth special formatting
        headers = dict(Authorization=f'Bearer {token}')

        req = cls._post_with_retries(
            cls.CHAT_POSTMESSAGE_ENDPOINT,
            json=payload,
            headers=headers,
        )
        if req is None:
            return None, None

        blob = req.json()
        # Slack returns status code 200 for many situations that constitute an error
        # Therefor checking for boolean *ok* in response instead
        if blob.get('ok'):
            thread_ts = blob.get('ts')
            return 200, thread_ts

        error = blob.get('error')

        # If req.status_code is None, we did not get a response from the slack API
        msg = f'Slack API returned {req.status_code} with error message `{error}`. Unable to Slack._send() with payload {payload}'
        S3Logger.error(msg)
        return 499, None

    # pylint: disable=too-many-arguments
    # pylint: disable=too-many-locals
    @classmethod
    def sendfile(
        cls,
        path_to_file: str,
        initial_comment: str,
        *,
        channel: str | None = None,
        mention: str | Iterable | None = None,
        parent_message: ParentMessage | None = None,
    ):
        """
        Upload a file to a slack channel

        :param: str :path_to_file:
        :param: str :initial_comment: Words in the message
        :param: str :channel: The channel where file will be uploaded.
                              Note channel is optional if Slack.default_channel
                              has already been set
                              Note channel is ignored if parent_message is set.

        :param str parent_message: (Optional)
                                   If present, send message as a reply to the parent message.
                                   (Start or continue a thread)

        :return: :bool:
        """
        # Allow user to pass `None` for parent message, without breaking when we access parent message attrubutes
        parent_message = (
            cls._empty_parent_message() if parent_message is None else parent_message
        )

        original_dir = os.getcwd()
        file_dir = os.path.dirname(path_to_file)
        basename = ntpath.basename(path_to_file)

        channel = parent_message.channel or channel or cls.default_channel
        if channel is None:
            raise cls.NoChannelError

        initial_comment = cls._mention(initial_comment, mention)

        # pylint: disable=pointless-string-statement
        """
        WORKAROUND

        When testing this on windows, if you attempt
        to directly upload a path like
        'C:\\stuff\\home room\\myfile.txt'
        it silently fails.

        Not sure if the issue is with the file
        being in a different directory (sometimes works)
        or if the issue is with the file
        being in a directory that has spaces in the name.

        But this workaround calls chdir() to get into the
        directory where the file lives, then calls chdir()
        again to get back to the original dir.
        """
        try:
            if file_dir:
                # Skip this step when file_dir is an empty string
                # (When file is in your current working directory)
                # because otherwise raises an error
                os.chdir(file_dir)
            with open(basename, 'rb') as ff:
                file_tuple = (basename, ff, '.csv')
                files_dict = dict(file=file_tuple)

                params = dict(
                    channels=[channel],
                    initial_comment=initial_comment,
                    thread_ts=parent_message.thread_ts,
                )

                status, thread_ts = cls._sendfile(params, files_dict)

        finally:
            os.chdir(original_dir)

        if status == 200:
            # return original parent_message if present and truthy
            return parent_message or ParentMessage(channel, thread_ts)

        # If status is None, we did not get a response from the slack API
        msg = f'Slack API returned {status}, Unable to send slack file with path "{path_to_file}" and channel "{channel}"'
        S3Logger.error(msg)
        return ParentMessage(channel, None)

    @classmethod
    def _sendfile(cls, params: dict, files_dict: dict[str, tuple]) -> tuple:
        """
        Actual post happens here, with retrying
        """
        token = Util.api_token()
        if token is None:
            # 503 Service is temporarily unavailable
            # You will end up here if there is an error fetching the token
            # Check logs to see exception type
            return 503, None

        params['token'] = token

        # With this SENDFILE_ENDPOINT, token is included in params
        req = cls._post_with_retries(
            cls.SENDFILE_ENDPOINT,
            params=params,
            files=files_dict,
        )
        if req is None:
            return None, None

        thread_ts = None
        status_code = req.status_code
        if status_code == 200:
            thread_ts = cls._thread_ts_from_sendfile_req(req)
        return req.status_code, thread_ts

    @classmethod
    def _thread_ts_from_sendfile_req(cls, req: Response) -> str | None:
        """
        Given a request object from posting to the SENDFILE_ENDPOINT,
        returns the first thread_ts it finds in the json blob.

        BACKGROUND
        When a file is uploaded, it contains data about multiple share channels.
        Presumably when the file is uploaded from slack2 it only has one share channel,
        so this method does the simplest thing and returns the first thread_ts it finds.

        :return: :str:
        """
        fkey = 'file'
        blob = req.json()
        if fkey not in blob:
            # On March 9, 2022 the slack API was experiencing issues where the
            # file was uploaded successfully but the payload coming back was
            # incorrect. In this case, we return None which means messages sent
            # to this thread_ts will appear in the main channel instead of the
            # thread.
            LOGGER.error('API unable to retrieve thread_ts from uploaded file')
            return None
        shares_blob = blob[fkey]['shares']
        for itemized_shares in shares_blob.values():
            for shares in itemized_shares.values():
                for share in shares:
                    timestamp = share.get('ts')
                    if timestamp:
                        return timestamp
        return None

    @classmethod
    def started(cls, filename: str) -> None:
        """
        Sends a slack indicating that the provided filename started
        """
        cls.send(f'Started {filename}')

    @classmethod
    def completed(cls, filename: str) -> None:
        """
        Sends a slack indicating that the provided filename completed
        """
        cls.send(f'Completed {filename}')

    @classmethod
    def _escape(cls, text: str) -> str:
        """
        Slack needs certain characters replaced with their html alternatives
        See https://api.slack.com/reference/surfaces/formatting#escaping

          - Replace the ampersand, &, with &amp;
          - Replace the less-than sign, < with &lt;
          - Replace the greater-than sign, > with &gt;

        """
        text = text.replace('&', '&amp;')
        text = text.replace('<', '&lt;')
        text = text.replace('>', '&gt;')
        return text

    @classmethod
    def _post_with_retries(cls, endpoint: str, **kwargs: dict) -> Response:
        """
        Call requests.post with the provided arguments.

        Retry any failures with tenacity, but in the case of persistent failure,
        DO NOT raise an exception.

        (Using the @tenacity.retry decorator raises an exception
        in the case of persistent failure, but we didn't want any
        exceptions raised so using this.


        This also has the added benefit that we could override cls.KWARGS
        at runtime.)

        See https://tenacity.readthedocs.io/en/latest/#retrying-code-block
        """
        try:
            for attempt in Retrying(**cls.KWARGS):
                with attempt:
                    return cls._post(endpoint, **kwargs)
        # XXX What does this mean by returning RetryError
        except RetryError:
            return None

    @classmethod
    def _post(cls, endpoint: str, **kwargs: dict) -> Response:
        """
        Call requests.post with the provided arguments.

        Note this is in a separate method from _post_with_retries
        so that when testing we can test the behaviour independend
        of tenacity
        """
        kwargs_with_timeout = {**kwargs, 'timeout': cls.TIMEOUT_SECONDS}
        req = requests.post(endpoint, **kwargs_with_timeout)  # noqa: S113
        body = req.json()
        # The slack API appears to return 200 status code regardless of success or failure
        # However it does include keys "ok" and "error" in the body
        if body.get('ok') is False and body.get('error') == 'invalid_auth':
            # Looks like we used the wrong token.
            # Raise a particular exception that will trigger a token re-fetch
            raise InvalidTokenError
        return req

    @classmethod
    def _empty_parent_message(cls):
        """
        Return an empty parent message so that its attributes can be accessed
        """
        return ParentMessage(None, None)

    @classmethod
    def _send_token_reply(cls, *, parent_message):
        """
        Send a short reply to parent_message

        This is to ensure that the message shows up in the "Threads" view in slack GUI
        """
        cls.send(TOKEN_REPLY_MESSAGE, parent_message=parent_message)


if __name__ == '__main__':
    # Converts dict to string
    # Slack.send(dict(testing='EarlyVote Slack'))

    # Slack.sendfile('config/slack_secrets.json-EXAMPLE', 'from sendfile()')
    # https://slack.com/api/chat.postMessage?token=TOKEN&channel=C01A8148LUX&text=hello%20from%20web%20tester&pretty=1

    logging.basicConfig(level=logging.INFO, datefmt='%H:%M:%S')

    Slack.default_channel = 'jack-test'

    Slack.send_at('Hi <http://example.com|there>', escape=False)
    parent_1 = Slack.send('Default channel w mention', mention='U0181U8BJ93')
    Slack.send_at('send_at with two mentions', mention=['U0181U8BJ93', 'UFY7ZCMP0'])
    parent_2 = Slack.send('Alternate channel', channel='jack-test2')
    Slack.send('Replying in Thread', channel='jack-test2', parent_message=parent_2)

    # Update Message
    parent_3 = Slack.send('Initial Content')
    Slack.update(
        'Updated Content',
        channel_id='C01A8148LUX',
        thread_ts=parent_3.thread_ts,
        escape=True,
    )

    # Update comment to uploaded file
    upload_msg = Slack.sendfile(
        'setup.py',
        'Original Comment',
    )
    Slack.update(
        'New comment',
        channel_id='C01A8148LUX',
        thread_ts=upload_msg.thread_ts,
    )

    # Simple Case w File in a different directory AND reply in thread AND mention
    Slack.sendfile(
        'files/one.txt',
        'Testing a file in a different directory',
        mention='U0181U8BJ93',
        parent_message=parent_1,
    )
