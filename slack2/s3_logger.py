from __future__ import annotations

import logging
import os
import platform
import socket
from datetime import datetime

import boto3
from botocore.exceptions import ClientError

PYTHON_LOGGER = logging.getLogger('slack2')


class S3Logger:
    """
    A centralized logger where all deployments of slack2 write to the same bucket.
    This is intended to help to understand what the common failure methods are,
    and how to work around them.
    """

    BUCKET = 'ts-slack2-logs'
    REGION = 'us-east-1'

    WARNING = 'warning'
    ERROR = 'error'

    UTF8 = 'utf-8'

    @classmethod
    def warning(cls, message):
        cls._write(message, cls.WARNING)

    @classmethod
    def error(cls, message):
        cls._write(message, cls.ERROR)

    @classmethod
    def _write(cls, message, level):
        # Additionally send the  message to the python logger so developers see the error
        # (Note using PYTHON_LOGGER instead of print() or logging.info)
        getattr(PYTHON_LOGGER, level)(f'S3Logger: {message}')

        tstamp, key = cls._timestamp_filename(level)
        mid = cls._machine_identifier()
        message = f'{tstamp} {mid} {message}\n'
        s3 = boto3.client('s3', region_name=cls.REGION)
        try:
            s3.put_object(
                Body=bytes(message, encoding=cls.UTF8),
                Bucket=cls.BUCKET,
                Key=key,
            )
        except ClientError:
            # You will end up here if not authorized to write to bucket
            PYTHON_LOGGER.error(
                f'Slack2 unable to write logs to S3 bucket "{cls.BUCKET}"'
            )

    @classmethod
    def _timestamp_filename(cls, level):
        """
        Returns a string like this:
            2021-12-12T10:22:33.456789.info
        """
        now = datetime.now()
        tstamp = now.strftime('%Y-%m-%d %H:%M:%S.%f')
        fname = tstamp.replace(':', '').replace('-', '').replace(' ', '_')
        fname = f'{fname}.{level.lower()}'
        return tstamp, fname

    @classmethod
    def _machine_identifier(cls):
        # Operating System
        os_ = platform.system()

        # Hostname
        node = platform.node()

        # IP Address
        _host = socket.gethostname()

        try:
            ip = socket.gethostbyname(_host)
        except socket.gaierror:
            # Sometimes my laptop raises socket.gaierror
            ip = 'unknown'

        # Running inside AWS Lambda
        lambda_ = 'lambda' if os.environ.get('AWS_EXECUTION_ENV') else ''

        # Current working directory
        cwd = os.getcwd()

        return f'{os_}|{lambda_}|{node}|{ip}|{cwd}'


if __name__ == '__main__':
    S3Logger.warning('Hello')
